//
//  PaymentsMO+CoreDataProperties.swift
//  
//
//  Created by Irina Romas on 15/10/2017.
//
//

import Foundation
import CoreData


extension PaymentsMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PaymentsMO> {
        return NSFetchRequest<PaymentsMO>(entityName: "Payments")
    }

    @NSManaged public var cwater: Int64
    @NSManaged public var date: NSDate?
    @NSManaged public var hwater: Int64
    @NSManaged public var sum: Double
    @NSManaged public var t1: Int64
    @NSManaged public var t2: Int64

}
