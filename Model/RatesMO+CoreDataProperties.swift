//
//  RatesMO+CoreDataProperties.swift
//  
//
//  Created by Irina Romas on 15/10/2017.
//
//

import Foundation
import CoreData


extension RatesMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RatesMO> {
        return NSFetchRequest<RatesMO>(entityName: "Rates")
    }

    @NSManaged public var cwater: Double
    @NSManaged public var drainage: Double
    @NSManaged public var hwater: Double
    @NSManaged public var t1: Double
    @NSManaged public var t2: Double

}
