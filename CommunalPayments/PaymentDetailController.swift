//
//  PaymentDetailController.swift
//  CommunalPayments
//
//  Created by Irina Romas on 27/10/2017.
//  Copyright © 2017 Irina Romas. All rights reserved.
//

import UIKit
import Eureka
import CoreData

class PaymentDetailController: FormViewController {
    
    var payment: PaymentsMO!
    var paymentLast: PaymentsMO?
    var rate: RatesMO!
    
    var cwaterP = 0
    var hwaterP = 0
    var t1P = 0
    var t2P = 0

    override func viewDidLoad() {
        
        cwaterP = Int(payment.cwater)
        hwaterP = Int(payment.hwater)
        t1P = Int(payment.t1)
        t2P = Int(payment.t2)
        
        let dateCurrent = Date()
        
        self.readRates()
        
        super.viewDidLoad()
        
        form +++ Section()
            <<< DateRow(){ row in
                row.title = "Дата"
                row.value = dateCurrent
                row.tag = "dateCurrent"
                row.value = payment.date
                }.onChange({ row in
                    if row.value != nil {
                        self.saveData()
                    }
                })
            
            +++ Section("Вода")
            <<< IntRow(){ row in
                row.title = "ХВС"
                row.tag = "cwater"
                row.value = cwaterP
                }.onChange({ row in
                    if row.value != nil {
                        self.cwaterP = Int(row.value!)
                        self.updateSum()
                    }
                })
                
            <<< IntRow(){ row in
                row.title = "ГВС"
                row.tag = "hwater"
                row.value = Int(payment.hwater)
                }.onChange({ row in
                    if row.value != nil {
                        self.hwaterP = Int(row.value!)
                        self.updateSum()
                    }
                })
                
            +++ Section("Электричество")
            <<< IntRow(){ row in
                row.title = "Т1"
                row.tag = "t1"
                row.value = Int(payment.t1)
                }.onChange({ row in
                    if row.value != nil {
                        self.t1P = Int(row.value!)
                        self.updateSum()
                    }
                })
                
            <<< IntRow(){ row in
                row.title = "Т2"
                row.tag = "t2"
                row.value = Int(payment.t2)
                }.onChange({ row in
                    if row.value != nil {
                        self.t2P = Int(row.value!)
                        self.updateSum()
                    }
                })
                
            +++ Section("")
            <<< DecimalRow(){ row in
                row.title = "Всего"
                row.tag = "sum"
                row.value = payment.sum
                }
    }
    
    func saveData() {
        
        let resultRowSum = form.rowBy(tag: "sum") as! DecimalRow
        let resultRowCWater = form.rowBy(tag: "cwater") as! IntRow
        let resultRowHWater = form.rowBy(tag: "hwater") as! IntRow
        let resultRowT1 = form.rowBy(tag: "t1") as! IntRow
        let resultRowT2 = form.rowBy(tag: "t2") as! IntRow
        let resultRowDate = form.rowBy(tag: "dateCurrent") as! DateRow
        
        if resultRowSum.value == nil || resultRowCWater.value == nil || resultRowHWater.value == nil || resultRowT1.value == nil || resultRowT2.value == nil {
            let alertController = UIAlertController(title: "Oops", message: "We can't proceed because one of the fields is blank. Please note that all fields are required.", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(alertAction)
            present(alertController, animated: true, completion: nil)
        }
        
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate) {
            
            payment.sum = resultRowSum.value!
            payment.cwater = Int64(resultRowCWater.value!)
            payment.hwater = Int64(resultRowHWater.value!)
            payment.t1 = Int64(resultRowT1.value!)
            payment.t2 = Int64(resultRowT2.value!)
            payment.date = resultRowDate.value!
            
            print("Saving data to context ...")
            appDelegate.saveContext()
            
        }
        
        
        dismiss(animated: true, completion: nil)

        
    }
    
    func updateSum() {
        var sum = 0.0
        let cwaterLast = Int(self.paymentLast?.cwater ?? 0)
        let hwaterLast = Int(self.paymentLast?.hwater ?? 0)
        let t1Last = Int(self.paymentLast?.t1 ?? 0)
        let t2Last = Int(self.paymentLast?.t2 ?? 0)
        
        let resultRow = form.rowBy(tag: "sum") as! DecimalRow
        sum = Double(cwaterP - cwaterLast) * self.rate.cwater + Double(hwaterP - hwaterLast) * self.rate.hwater
        sum = sum + Double(cwaterP - cwaterLast + hwaterP - hwaterLast) * self.rate.drainage + Double(t1P - t1Last) * self.rate.t1 + Double(t2P-t2Last) * self.rate.t2
        resultRow.value = Double(sum)
        resultRow.updateCell()
        
        self.saveData()
        
    }
    
    func readRates() {
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate) {
            let req = NSFetchRequest<RatesMO>(entityName: "Rates")
            
            do {
                let rates = try appDelegate.persistentContainer.viewContext.fetch(req)
                if let rate = rates.first {
                    self.rate = rate
                } else {
                    self.rate = RatesMO.createWithAllZeros()
                }
            } catch let e {
                print(e)
            }
        }
    }
    
    func readLastRecord() {
        
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate) {
            let req = NSFetchRequest<PaymentsMO>(entityName: "Payments")
            
            do {
                let payments = try appDelegate.persistentContainer.viewContext.fetch(req)
                if let payment = payments.last {
                    self.paymentLast = payment
                }
            } catch let e {
                print(e)
            }
        }
        
    }

}
