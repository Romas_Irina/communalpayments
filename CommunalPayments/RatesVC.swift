//
//  Rates.swift
//  CommunalPayments
//
//  Created by Irina Romas on 14/10/2017.
//  Copyright © 2017 Irina Romas. All rights reserved.
//

import UIKit
import Eureka
import CoreData

class RatesVC: FormViewController {
    
    var rate: RatesMO!
    
//    var rates: [RatesMO] = []
//    var cwater = 0.0
//    var hwater = 0.0
//    var drainage = 0.0
//    var t1 = 0.0
//    var t2 = 0.0
   
    override func viewDidLoad() {
        
        self.readRates()
        
//        if rates.count > 0 {
//            cwater = rates[0].cwater
//            hwater = rates[0].hwater
//            drainage = rates[0].drainage
//            t1 = rates[0].t1
//            t2 = rates[0].t2
//        }
        
        super.viewDidLoad()
        

        form +++ Section()
            
            +++ Section("Вода")
            <<< DecimalRow(){ row in
                row.title = "ХВС"
                row.value = self.rate.cwater
                }.onChange({ row in
                    if row.value != nil {
                        self.rate.cwater = row.value!
                        self.updateRates()
                    }
                    })
            <<< DecimalRow() { row in
                row.title = "ГВС"
                row.value = self.rate.hwater
                }.onChange({ row in
                    if row.value != nil {
                        self.rate.hwater = row.value!
                        self.updateRates()
                    }
                })
            <<< DecimalRow() { row in
                row.title = "водоотведение"
                row.value = self.rate.drainage
                }.onChange({ row in
                    if row.value != nil {
                        self.rate.drainage = row.value!
                        self.updateRates()
                    }
                })
            
            +++ Section("Электричество")
            <<< DecimalRow() { row in
                row.title = "Т1"
                row.value = self.rate.t1
                }.onChange({ row in
                    if row.value != nil {
                        self.rate.t1 = row.value!
                        self.updateRates()
                    }
                })
            <<< DecimalRow(){ row in
                row.title = "Т2"
                row.value = self.rate.t2
                }.onChange({ row in
                    if row.value != nil {
                        self.rate.t2 = row.value!
                        self.updateRates()
                    }
                })
        
        }

    func updateRates() {
        try? rate.managedObjectContext?.save()
//                let req = NSFetchRequest<RatesMO>(entityName: "Rates")
//                req.fetchLimit = 1
//
//                do {
//                    if let result = (try appDelegate.persistentContainer.viewContext.fetch(req)).first {
//                        result.cwater = self.rate.cwater
//                        result.hwater = self.rate.hwater
//                        result.drainage = self.rate.drainage
//                        result.t1 = self.rate.t1
//                        result.t2 = self.rate.t2
//                    } else {
//                        let r = RatesMO(context: appDelegate.persistentContainer.viewContext)
//                        r.cwater = self.rate.cwater
//                        r.hwater = self.rate.hwater
//                        r.drainage = self.rate.drainage
//                        r.t1 = self.rate.t1
//                        r.t2 = self.rate.t2
//                    }
//                } catch let e {
//                    print(e)
//                }
//                appDelegate.saveContext()
//            }
    }
    
    func readRates() {
        if let appDelegate = (UIApplication.shared.delegate as? AppDelegate) {
            let req = NSFetchRequest<RatesMO>(entityName: "Rates")
            
            do {
                let rates = try appDelegate.persistentContainer.viewContext.fetch(req)
                if let rate = rates.first {
                    self.rate = rate
                } else {
                    self.rate = RatesMO.createWithAllZeros()
                }
            } catch let e {
                print(e)
            }
        }
    }
}

extension RatesMO {
    convenience init(cwater: Double, hwater: Double, drainage: Double, t1: Double, t2: Double) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let d = NSEntityDescription.entity(forEntityName: "Rates", in: appDelegate.persistentContainer.viewContext)!
        self.init(entity: d, insertInto: appDelegate.persistentContainer.viewContext)
        
        self.cwater = cwater
        self.hwater = hwater
        self.drainage = drainage
        self.t1 = t1
        self.t2 = t2
    }
    
    static func createWithAllZeros() -> RatesMO {
        return RatesMO(cwater: 0, hwater: 0, drainage: 0, t1: 0, t2: 0)
    }
}
